const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
//const hand =[];
//let hand = new Array(); 
//let total=0; 

let handLength=0; 
let total=0;
 let haveAce=false;
let isSoft=false; 
let isDealerDraw=false; 
//const cardPlayerInstance=[]; 



class CardPlayer {
 
  constructor(name)
  {
    this.name=name;
    //this.hand=[]; 
    // const hand={
    //   suit:blackjackDeck,
    //   val:0,
    //   displayVal:0
    // };
  }
  
   hand=[]; 
   drawCard = function() {

    const randomLocation= Math.floor((Math.random() * blackjackDeck.length));
    console.log(`Random Location of the Card Drawn: ${randomLocation}`);
    //console.log(`Card Drawn1: ${blackjackDeck[randomLocation].displayVal}`);
    // hand.push(blackjackDeck.filter((card)=> cards[randomLocation]));
    //hand.push(blackjackDeck[randomLocation]);
   // console.log(`value: ${blackjackDeck[randomLocation].val}`);
    this.hand.push({
      suit:blackjackDeck[randomLocation].suit,
      val:blackjackDeck[randomLocation].val,
      displayVal:blackjackDeck[randomLocation].displayVal
    })
    handLength=this.hand.length;
    for (let i=0;i<this.hand.length;i++)
    {
      if (i===0)
      {
    console.log(`Cards Drawn for dealer: ${this.hand[i].displayVal}`);
      }
      if (i===1)
      {
    console.log(`Cards Drawn for player: ${this.hand[i].displayVal}`);
      }
    };
    return this.hand;
  }
   
}; //TODO

// CREATE TWO NEW CardPlayers
const dealer= new CardPlayer("dealer"); // TODO
const player= new CardPlayer("player"); // TODO

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
const calcPoints = (hand) => {
  // CREATE FUNCTION HERE
  //console.log(`hand: ${hand[0].val}`);
  
    //console.log(`hand length: ${handLength}`)
    //console.log(`hand: ${hand[0]}`);
 
 hand.forEach((card)=>
 {
   if(card.displayVal.toLowerCase()==='ace')
   {
     total+=1;
     haveAce=true;
   }
   else{
     total+=card.val; 
   }
 }
 );

 if(haveAce && total<=11)
 {
   total+=10; 
   isSoft=true; 
 }
  //  for (let i=0;i<handLength;i++)
  //   {
  //     if(hand[i].displayVal==='ace')
  //     {
  //       total += 1;
  //     }
  //     else
  //     {
  //    // console.log(`hand: ${hand[i].val}`);
  //     total += hand[i].val;
  //     }
  //   //console.log(`Cards Drawn ${i+1}: ${hand[i].displayVal}`);
  //   };
  console.log (`total = ${total}`);
  console.log(`isSoft: ${isSoft}`)
  return total; 
};

/**
 * Determines whether the dealer should draw another card.
 * 
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = (dealerHand) => {
  // CREATE FUNCTION HERE
  
  let dealerPoints=calcPoints(dealerHand);
  
  if(dealerPoints<=16)
  {
    isDealerDraw=true; 
  }
  else if (dealerPoints===17 & haveAce===true)
  {
    isDealerDraw=true; 
  }
  else
  {
    isDealerDraw=false; 
  }

  console.log(`dealer should draw another card: ${isDealerDraw}`);
  return isDealerDraw;

}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  // CREATE FUNCTION HERE
  // string who=`Player Score`
  
  if (playerScore===dealerScore)
  {
    console.log(`It's a tie between dealer and player`);
  }
  else
  {
    if ((playerScore>dealerScore) && playerScore<21)
    {
      console.log(`Game Over! Player Wins.`);
    }
    if ((playerScore<dealerScore) && dealerScore<21)
    {
      console.log(`Game Over! Dealer Wins.`);
    }
   
  }

  // if(hand.val===21)
  // {
  //   console.log(`${player.name} got 21. `);
  // }
  // else if (hand.val>21)
  // {

  //  console.log(`${player.name} got Busted. Your total is `);
  // }
    console.log(`Winner is:`);
  // return  scores;
}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`;
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    console.log(`Player Score: ${playerScore}`);
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  let dealerScore = calcPoints(dealer.hand).total;
  console.log(`isDealerDraw ${isDealerDraw}`);
  while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'Dealer went over 21 - you win!';
  }
  console.log(`Dealer stands at ${dealerScore}`);

  return determineWinner(playerScore, dealerScore);
}
 console.log(startGame());